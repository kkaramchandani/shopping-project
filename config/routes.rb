Rails.application.routes.draw do
  get '/search-products' => 'products#search'
  post '/category' => 'products#category'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get 'products/new-review/:product_id' => 'reviews#new'
  resources :reviews
  resources :product_images
  # devise_for :users
  devise_for :users, controllers: { confirmations: 'confirmations' }
  root 'products#new'
  get '/products/delete/:id' => 'products#destroy'
  resources :products
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
