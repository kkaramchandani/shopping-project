class ProductsController < ApplicationController
  before_action :set_product, only: %i[ show edit update destroy ]

  def search
    @products = Product.all
  end

  def category
     @category = params[:category]   
     @products = Product.where('category' => @category)
  end

  def index
    @products = Product.paginate(page: params[:page], per_page: 5)
  end

  def show    
  end

  def new
    @product = Product.new
  end

  def edit
  end

  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        params[:product_images]['image'].each do |a|
          @product_image = @product.product_images.create!(:image => a)
       end
        format.html { redirect_to @product, notice: "Product was successfully created." }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: "Product was successfully updated." }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: "Product was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    def set_product
      @product = Product.find(params[:id])
    end

    def product_params
      params.require(:product).permit(:title, :description, :price, :category ,product_images_attributes: [:id, :product_id, :image])
    end
end
