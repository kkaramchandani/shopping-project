ActiveAdmin.register Product do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  form partial: 'form'
  form(html: { multipart: true }) do |f|
    f.inputs "Product" do
      f.input :title
      f.input :description
      f.input :price
      f.input :image , as: :file,input_html: { multiple: true } 
      f.label :category
      f.select :category, [ 'Food', 'Laptop', 'Refrigerator', 'Television', 'Computer', 'PC Accessories', 'Clothes', 'Camera', 'Toys', 'Other' ], :prompt => 'Select One'
    end
    f.submit
  end
  permit_params :title, :description, :price, :category, image: []
  #
  # or
  #
  # permit_params do
  #   permitted = [:title, :description, :price]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
