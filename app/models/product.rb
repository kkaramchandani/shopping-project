class Product < ApplicationRecord
    validates :title, presence:true, length: {minimum:3, maximum:50}
    validates :price, presence:true, :numericality =>{:greater_than_or_equal_to => 1}
    validates :description, presence:true, length: {minimum:10, maximum:200}
    serialize :image, Array
    mount_uploaders :image, ImageUploader    
    has_many :reviews
end
