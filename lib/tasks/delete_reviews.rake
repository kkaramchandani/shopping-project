namespace :delete do
    desc "Task to delete reviews everyday"
    task :reviews => :environment do
       Review.where("rating < ?", 3).destroy_all
    end
  end